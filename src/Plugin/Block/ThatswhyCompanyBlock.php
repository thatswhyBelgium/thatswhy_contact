<?php
/**
 * @file
 * A custom block for this site
 */
namespace Drupal\thatswhy_contact\Plugin\Block;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Company block'
 * @Block(
 *   id = "thatswhy_contact_company_block",
 *   admin_label = @Translation("Company block"),
 *   category = @Translation("Contact blocks")
 * )
 */
class ThatswhyCompanyBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $company_config = \Drupal::config('thatswhy_contact.company');
    $block_config = $this->getConfiguration();
    $system_config = \Drupal::config('system.site');

    $content = '';
    $items = $this->contact_items();

    $divider = FALSE;
    $divider_symbol = $this->dividers();
    $divider_symbol = $divider_symbol[$block_config['divider']]['symbol'];

    if ($block_config['show_sitename'] == TRUE) {
      $divider = TRUE;
      $content .= $this->wrap('<strong>' . $system_config->get('name') . '</strong>', 'sitename');
    }

    if ($block_config['show_siteslogan'] == TRUE && !empty($system_config->get('slogan'))) {
      if ($divider == TRUE) {
        $content .= ' ' . $divider_symbol . ' ';
      }

      $divider = TRUE;
      $system_config = \Drupal::config('system.site');
      $content .= $this->wrap($system_config->get('slogan'), 'slogan');
    }

    if ($block_config['show_sitemail'] == TRUE) {
      $system_config = \Drupal::config('system.site');
      $email = '<a href="mailto:'. $system_config->get('mail') . '" title="' . t('Send us a mail') . '">' . $system_config->get('mail') . '</a>';

      if ($block_config['show_sitemail_at_end'] == FALSE) {
        if ($divider == TRUE) {
          $content .= ' ' . $divider_symbol . ' ';
        }
        $divider = TRUE;

        $content .= $this->wrap($email, 'email');
      }
    }

    foreach ($items as $key => $value) {
      if ($block_config['show_' .$key] == TRUE) {
        if (!empty($company_config->get($key))) {
          if ($divider == TRUE && $key != 'city') {
            $content .= ' ' . $divider_symbol . ' ';
          } elseif ($key == 'city' && $divider == TRUE) {
            $content .= ' ';
          }

          if ($key != 'city') {
            if ($key == 'postal_code') {
              $content .= $this->wrap($company_config->get($key) . ' ' . $company_config->get('city'), $key);
            } else {
              if(strpos($key, '_mail')) {
                $key_content = '<a href="mailto:'. $system_config->get($key) . '" title="' . t('Send us a mail') . '">' . $system_config->get($key) . '</a>';
              } else {
                $key_content = $company_config->get($key);
              }
              $content .= $this->wrap($key_content, $key);
            }
          }


          if ($divider == FALSE) {
            $divider = TRUE;
          }
        }
      }
    }

    if ($block_config['show_sitemail_at_end'] == TRUE && isset($email)) {
      if ($divider == TRUE) {
          $content .= ' ' . $divider_symbol . ' ';
        }
        $divider = TRUE;

        $content .= $this->wrap($email, 'email');
    }

    return array(
      '#markup' => $content ,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['thatswhy_contact_company_general'] = array(
      '#type' => 'details' ,
      '#title' => t('General settings') ,
      '#collapsible' => TRUE ,
      '#collapsed' => FALSE ,
    );

    $form['thatswhy_contact_company_general']['show_sitename'] = array(
      '#type' => 'checkbox' ,
      '#title' => $this->t('Show site name first') ,
      '#default_value' => isset($config['show_sitename']) ? $config['show_sitename'] : TRUE ,
    );

    $form['thatswhy_contact_company_general']['show_siteslogan'] = array(
      '#type' => 'checkbox' ,
      '#title' => $this->t('Show site slogan') ,
      '#default_value' => isset($config['show_siteslogan']) ? $config['show_siteslogan'] : TRUE ,
    );

    $form['thatswhy_contact_company_general']['show_sitemail'] = array(
      '#type' => 'checkbox' ,
      '#title' => $this->t('Show site e-mail') ,
      '#default_value' => isset($config['show_sitemail']) ? $config['show_sitemail'] : TRUE ,
    );

    $form['thatswhy_contact_company_general']['show_sitemail_at_end'] = array(
      '#type' => 'checkbox' ,
      '#title' => $this->t('Show site e-mail as last item') ,
      '#default_value' => isset($config['show_sitemail_at_end']) ? $config['show_sitemail_at_end'] : FALSE ,
    );

    $form['thatswhy_contact_company_general']['divider'] = array(
      '#type' => 'select' ,
      '#title' => $this->t('Divider') ,
      '#description' => $this->t("The divider will divide the different items in this block.") ,
      '#options' => $this->dividers_select() ,
      '#default_value' => isset($config['divider']) ? $config['divider'] : '' ,
      '#required' => TRUE ,
    );

    $form['thatswhy_contact_company_items'] = array(
      '#type' => 'details' ,
      '#title' => t('Specific item settings') ,
      '#collapsible' => TRUE ,
      '#collapsed' => FALSE ,
    );

    foreach ($this->contact_items() as $key => $value) {
      $form['thatswhy_contact_company_items']['show_' . $key] = array(
        '#type' => 'checkbox' ,
        '#title' => $this->t('Show @item', array('@item' => $value, )) ,
        '#default_value' => isset($config['show_' .$key]) ? $config['show_' .$key] : TRUE ,
      );
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['show_sitename'] = $form_state->getValue(array('thatswhy_contact_company_general','show_sitename'));
    $this->configuration['show_siteslogan'] = $form_state->getValue(array('thatswhy_contact_company_general','show_siteslogan'));
    $this->configuration['show_sitemail'] = $form_state->getValue(array('thatswhy_contact_company_general','show_sitemail'));
    $this->configuration['show_sitemail_at_end'] = $form_state->getValue(array('thatswhy_contact_company_general','show_sitemail_at_end'));
    $this->configuration['divider'] = $form_state->getValue(array('thatswhy_contact_company_general','divider'));

    foreach ($this->contact_items() as $key => $value) {
      $this->configuration['show_' .$key] = $form_state->getValue(array('thatswhy_contact_company_items','show_' . $key));
    }
  }

  private function dividers() {
    return array(
      'space' => array(
        'name' => $this->t("Simple space") ,
        'symbol' => " " ,
      ) ,
      'vert_line' => array(
        'name' => $this->t("Vertical line: |"),
        'symbol' => '|',
      ),
      'underscore' => array(
        'name' => $this->t("Underscore: _"),
        'symbol' => '_',
      ),
      'hypen' => array(
        'name' => $this->t("Hyphen: -") ,
        'symbol' => '-',
      ),
      'slash_forward' => array(
        'name' => $this->t("Slash forward: /") ,
        'symbol' => '/',
      ),
      'slash_forward_double' => array(
        'name' => $this->t("Double slash forward: //") ,
        'symbol' => '//',
      ),
      'slash_backwards' => array(
        'name' => $this->t("Slash backwards: \\") ,
        'symbol' => '\\',
      ),
      'slash_backwards_double' => array(
        'name' => $this->t("Double slash backwards: \\\\") ,
        'symbol' => '\\',
      ),
    );
  }

  private function dividers_select() {
    $dividers = $this->dividers();

    $select_dividers = array();
    foreach ($dividers as $key => $details) {
      $select_dividers[$key] = $details['name'];
    }

    return $select_dividers;
  }

  private function wrap($content, $class = '', $element = 'span') {
    $element_class = "contact-item";
    if (!empty($class)) {
      $element_class = $element_class . ' ' . $class;
    }
    return '<'.$element.' class="' . $element_class . '">'.$content.'</'.$element.'>';
  }

  private function contact_items() {
    return array(
      'personal_name' => $this->t("Personal name") ,
      'address' => $this->t("Address") ,
      'postal_code' => $this->t("Postal code") ,
      'city' => $this->t("City") ,
      'phone_1' => $this->t("Phone number 1") ,
      'phone_2' => $this->t("Phone number 2") ,
      'extra_mail' => $this->t("Extra e-mail") ,
      'sales_mail' => $this->t('Sales e-mail'),
      'vat' => $this->t("VAT number") ,
    );
  }

  public function getCacheTags() {
    return Cache::mergeTags(parent::getCacheTags(), ['thatswhy_company_block']);
  }
}
