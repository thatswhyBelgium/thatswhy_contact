<?php
/**
 * @file
 * A custom block for this site
 */
namespace Drupal\thatswhy_contact\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Made by Thats Why block'
 * @Block(
 *   id = "thatswhy_contact_creator_block",
 *   admin_label = @Translation("Made by That's Why block"),
 *   category = @Translation("Custom blocks")
 * )
 */
class ThatswhyCreatorBlock extends BlockBase implements BlockPluginInterface {
  /**
   * {@inheritdoc}
   */
  public function build() {

    $base_path = '/' . drupal_get_path('module', 'thatswhy_contact');
    $block_config = $this->getConfiguration();
    if(empty($block_config["thatswhy_creator_logo_color"])) {
      $block_config["thatswhy_creator_logo_color"] = "tw-green";
    }

    return array(
      "#theme" => "thatswhy_contact_creator" ,
      "#tw_link" => "https://thatswhy.be" ,
      "#svg_image" => $base_path . '/assets/thatswhy-logo-' . $block_config["thatswhy_creator_logo_color"] . '.svg' ,
    );
  }

  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['thatswhy_creator_logo_color'] = array(
      "#type" => "radios" ,
      "#title" => $this->t("Select the color of the that's why logo") ,
      "#options" => array(
        "white" => $this->t("White") ,
        "tw-green" => $this->t("That's Why green") ,
        "black" => $this->t("Black") ,
      ) ,
      "#default_value" => isset($config["thatswhy_creator_logo_color"]) ? $config["thatswhy_creator_logo_color"] : "tw-green" ,
      "#required" => TRUE ,
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration["thatswhy_creator_logo_color"] = $form_state->getValue("thatswhy_creator_logo_color",'tw-green');
  }

  public function getCacheTags() {
    return Cache::mergeTags(parent::getCacheTags(), ['thatswhy_creator_block']);
  }
}
