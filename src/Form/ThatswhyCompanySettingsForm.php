<?php
/**
 * @file
 * This is the ThatswhyCompany admin include which provides an interface to Thats Why Extra to change some of the default settings
 * Contains \Drupal\thatswhy_contact\Form\SettingsForm.
 */

namespace Drupal\thatswhy_contact\Form;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form to configure module settings.
 */
class ThatswhyCompanySettingsForm extends ConfigFormBase {

  /**
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cacheInvalidator
   */
  protected $cacheInvalidator;

  public function __construct(
    \Drupal\Core\Config\ConfigFactoryInterface $config_factory,
    CacheTagsInvalidatorInterface $cache_invalidator
  ) {
    parent::__construct($config_factory);
    $this->cacheInvalidator = $cache_invalidator;
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cache_tags.invalidator')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'thatswhy_contact_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'thatswhy_contact.settings.company',
    ];
  }

    /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {

    // Get company settings
    $company_settings = $this->config('thatswhy_contact.company');

    $form['company'] = array(
      '#type' => 'details' ,
      '#title' => $this->t('Company data') ,
      '#collapsible' => TRUE ,
      '#collapsed' => FALSE ,
    );

    $form['company']['personal_name'] = array(
      '#type' => 'textfield' ,
      '#title' => $this->t('Personal name') ,
      '#description' => $this->t('You want to have a more personalised display in the company data.') ,
      '#default_value' => $company_settings->get('personal_name') ,
    );

    $form['company']['address'] = array(
      '#type' => 'textfield' ,
      '#title' => $this->t('Address') ,
      '#description' => $this->t('Street + number') ,
      '#default_value' => $company_settings->get('address') ,
    );

    $form['company']['postal_code'] = array(
      '#type' => 'textfield' ,
      '#title' => $this->t('Postal code') ,
      '#default_value' => $company_settings->get('postal_code') ,
    );

    $form['company']['city'] = array(
      '#type' => 'textfield' ,
      '#title' => $this->t('City') ,
      '#default_value' => $company_settings->get('city') ,
    );

    $form['company']['phone_1'] = array(
      '#type' => 'tel' ,
      '#title' => $this->t('Phone number / Fax 1') ,
      '#description' => $this->t('The first number to display') ,
      '#default_value' => $company_settings->get('phone_1') ,
    );

    $form['company']['phone_2'] = array(
      '#type' => 'tel' ,
      '#title' => $this->t('Phone number / Fax 2') ,
      '#description' => $this->t('The second number to display') ,
      '#default_value' => $company_settings->get('phone_2') ,
    );

    $form['company']['vat'] = array(
      '#type' => 'textfield' ,
      '#title' => $this->t('VAT number') ,
      '#description' => $this->t('The VAT number (f.e: BE0525.973.293)') ,
      '#default_value' => $company_settings->get('vat') ,
    );

    $form['company']['extra_mail'] = array(
      '#type' => 'email' ,
      '#title' => $this->t('Extra e-mail') ,
      '#description' => $this->t('Leave empty to use site email') ,
      '#default_value' => $company_settings->get('extra_mail') ,
    );

    $form['company']['sales_mail'] = array(
      '#type' => 'email' ,
      '#title' => $this->t('Sales e-mail') ,
      '#description' => $this->t('Leave empty to use site email') ,
      '#default_value' => $company_settings->get('sales_mail') ,
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   * Compares the submitted settings to the defaults and unsets any that are equal. This was we only store overrides.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Run over the settings and save them.
    $config = $this->configFactory->getEditable('thatswhy_contact.company');
    foreach ($form_state->getValues() as $key => $value) {
      $config->set($key, $value);
    }
    $config->save();

    // Clear cache after submit!
    $this->cacheInvalidator->invalidateTags(['thatswhy_company_block']);
  }
}
